var http = require('http');

function getAllPictures(callbackFunction) {
  var MongoClient = require('mongodb').MongoClient;
  var url = "mongodb://localhost:27017/";
  var l = [];
  MongoClient.connect(url, function(err, db) {
    if (err) { throw err; }
    var dbo = db.db("quinncia");
    dbo.collection("pictures").find({}).toArray(function(err, result) {
      if (err) { throw err; }
      for(var inc = 0; inc < result.length; inc++) {
        l.push(result[inc]["path"]);
      }
      callbackFunction(l);
      db.close();
    });
  });
}

//AUTHENTICATION ENDPOINT
http.createServer(function (request, response) {
  response.writeHead(200, {'Content-Type': 'text/plain'});
  //authentication endpoint
  var url = require('url');
  var url_parts = url.parse(request.url, true);
  var query = url_parts.query;

  var myCallback = function(data) {
    for(var inc = 0; inc < data.length; inc++) {
      response.write("<path>" + data[inc] + "</path>");
    }
    response.end();
  };

  getAllPictures(myCallback);

}).listen(2343);
console.log('photo album endpoint running at http://127.0.0.1:2343/');


