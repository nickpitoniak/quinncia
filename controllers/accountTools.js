//ENDPOINTS FOR HANDELING ACCOUNT CREATION AND LOGIN REQUESTS

var http = require("http");
var auth = "";
function addUserToDb(inputEmail, inputPassword) {
  var MongoClient = require('mongodb').MongoClient;
  var url = "mongodb://localhost:27017/";
//check count(getUserByEmailAddress(inputEmail)) == 0
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("quinncia");
    var myobj = { email: inputEmail, pword: inputPassword};
    dbo.collection("users").insertOne(myobj, function(err, res) {
      if (err) throw err;
      console.log("user object inserted into db");
      db.close();
    });
  });
}
function getAllUsers() {
  var MongoClient = require('mongodb').MongoClient;
  var url = "mongodb://localhost:27017/";
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("quinncia");
    dbo.collection("users").find({}).toArray(function(err, result) {
      if (err) throw err;
      console.log("\n\ncount: " + result.length + "\n\n");
      console.log(result);
      db.close();
    });
  });
}
function getUserByEmailAddress(inputEmailAddress, inputPword, callback) {
  var MongoClient = require('mongodb').MongoClient;
  var url = "mongodb://localhost:27017/";

  MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  var dbo = db.db("quinncia");
  var query = { email: inputEmailAddress };
  dbo.collection("users").find(query).toArray(function(err, result) {
    if (err) throw err;
    if(result.length == 0) {addUserToDb(inputEmailAddress, inputPword); callback("success"); } else { callback("email exists"); }
    db.close();
  });
  });
}
function login(uname, pword, callback) {
  var MongoClient = require('mongodb').MongoClient;
  var url = "mongodb://localhost:27017/";

  MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  var dbo = db.db("quinncia");
  var query = { email: uname, pword: pword };
  dbo.collection("users").find(query).toArray(function(err, result) {
    if (err) throw err;
    if(result.length > 0) { callback("success", uname); } else { callback("failure", uname); };
    db.close();
  });
  });
}
//CREATE ACCOUNT ENDPOINT
http.createServer(function (request, response) {
  response.writeHead(200, {'Content-Type': 'text/plain'});
  //authentication endpoint
  var url = require('url');
  var url_parts = url.parse(request.url, true);
  var query = url_parts.query;

  var handleDataCallback = function(data) {
    if(data == "success") {
      response.end("success");
    } else {
      response.end("email exists");
    }
  }
  //login(String(query.email), String(query.pword), handleDataCallback);
  getUserByEmailAddress(query.email, query.pword, handleDataCallback);
}).listen(8757);
console.log('create users endpoint running at http://127.0.0.1:8757/');

//GET ALL USERS ENDPOINT
http.createServer(function (request, response) {
   response.writeHead(200, {'Content-Type': 'text/plain'});
   getAllUsers()
}).listen(3232);
console.log('get all users endpoint running at http://127.0.0.1:3232/');

//AUTHENTICATION ENDPOINT
http.createServer(function (request, response) {
  response.writeHead(200, {'Content-Type': 'text/plain'});
  //authentication endpoint
  var url = require('url');
  var url_parts = url.parse(request.url, true);
  var query = url_parts.query;

  var handleDataCallback = function(data) {
    if(data == "success") {
      response.end("success");
    } else {
      response.end("failure");
    }
  }
  login(String(query.email), String(query.pword), handleDataCallback);
}).listen(3243);
console.log('login.js endpoint running at http://127.0.0.1:3243/');
